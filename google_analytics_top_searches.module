<?php

/**
 * @file
 * Module file for Google Analytics Top Searches
 *
 * @todo In order to generalize for contrib, make this work when search categories aren't used
 *   in googleanalytics settings.
 */

/**
 * Implements hook_menu()
 */
function google_analytics_top_searches_menu() {
  $items = array();
  $items['admin/config/search/google-analytics-top-searches'] = array(
    'page callback' => 'drupal_get_form',
    'page arguments' => array('google_analytics_top_searches_admin'),
    'title'=>'Google Analytics Top Searches Settings',
    'access arguments' => array('administer nodes'),
    'type' => MENU_NORMAL_ITEM,
  );
  if (!variable_get('googleanalytics_site_search')) {
    unset($items['admin/config/search/google-analytics-top-searches']['page arguments']);
    $items['admin/config/search/google-analytics-top-searches']['page callback'] = 'google_analytics_top_searches_error_page';
  }
  return $items;
}

/**
 * Callback function for admin/config/search/google-analytics-top-searches
 *
 * @return form structure
 */
function google_analytics_top_searches_admin() {
  $defaults = array();
  if (!$defaults['google_analytics_top_searches_number_searches'] = variable_get('google_analytics_top_searches_number_searches')) {
    $defaults['google_analytics_top_searches_number_searches'] = 10;
  }
  
  $defaults['google_analytics_top_searches_query_start_date'] = variable_get('google_analytics_top_searches_query_start_date');
  if (!isset($defaults['google_analytics_top_searches_query_start_date'])) {
    $defaults['google_analytics_top_searches_query_start_date'] = 2592000;
  }

  $defaults['google_analytics_top_searches_search_by_category'] = variable_get('google_analytics_top_searches_search_by_category');
  if (!isset($defaults['google_analytics_top_searches_search_by_category'])) {
    $defaults['google_analytics_top_searches_search_by_category'] = 1;
  }

  $form = array(
    'google_analytics_top_searches' => array(
      '#type' => 'fieldset',
      '#title' => t('Google Analytics Top Searches Settings'),

      'google_analytics_top_searches_number_searches' => array(
        '#type' => 'textfield',
        '#title' => t('Number of searches to display'),
        '#description' => t('Enter a number between 1 and 10,000.'),
        '#default_value' => $defaults['google_analytics_top_searches_number_searches'],
        '#required' => TRUE,
      ),

      'google_analytics_top_searches_query_start_date' => array(
        '#type' => 'select',
        '#title' => t('How far back in time should we query?'),
        '#default_value' => $defaults['google_analytics_top_searches_query_start_date'],
        '#options' => array(
          86400 => t('One Day'),
          604800 => t('One Week'),
          2592000 => t('One Month'),
          31536000 => t('One Year'),
          0 => t('Forever'),
        ),
      ),
    ),
  );
  if (variable_get('googleanalytics_category_tracking_pages')) {
    $form['google_analytics_top_searches']['google_analytics_top_searches_search_by_category'] = array(
      '#type' => 'radios',
      '#title' => t('Display Top Searches By Search Category'),
      '#description' => t('Google Analytics divides the search forms on your site into categories. If you would like to display only searches performed in the particular search form, and not display the site-wide total, check yes.'),
      '#default_value' => $defaults['google_analytics_top_searches_search_by_category'],
      '#options' => array(
        0 => t('No'),
        1 => t('Yes')
      ),
    );
  }
  return system_settings_form($form);
}

/**
 * Alternate callback function for admin/config/search/google-analytics-top-searches
 * This displays an error message in the case that this module is enabled
 * but the googleanalytics_site_search variable isn't set in the googleanalytics module.
 *
 * @return string
 *  Error message html
 */
function google_analytics_top_searches_error_page() {
  $message = t('You\'ll want to head over to the <a href="@url">Google Analytics configuration page</a> and enable tracking for internal search before using this module!', array('@url' => url('admin/config/system/googleanalytics')));
  $out = '<h2>' . t('Hold on there!') . '</h2>';
  $out .= '<p>' . $message . '</p>';
  return $out;
}

/**
 * Implements hook_block_info
 */
function google_analytics_top_searches_block_info() {
  $blocks['google_analytics_top_searches'] = array (
    'info' => t('Google Analytics Top Searches'),
  );
  return $blocks;
}

/**
 * Implements hook_block_view
 */
function google_analytics_top_searches_block_view($delta = '') {
  $block = array();
  switch ($delta) {
    case 'google_analytics_top_searches':
      $block['subject'] = t('Top Searches');
      $block['content'] = _google_analytics_top_searches_get_html();
      break;
  }
  return $block;
}

/**
 * Callback function for block google_analytics_top_searches
 *
 * Entry point to getting top search results from Google Analytics API
 *
 * @return string rendered html unordered list of results
 */
function _google_analytics_top_searches_get_html() {
  $out = &drupal_static(__FUNCTION__);
  if (!isset($out)) {
    if($search_categories = _google_analytics_top_searches_get_search_categories()) {
      $path = filter_xss(current_path());
      foreach ($search_categories as $search_category_category => $search_category_path) {
        if (drupal_match_path($path, $search_category_path)) {
          if ($cache = cache_get('google_analytics_top_searches_' . drupal_html_class($search_category_category))) {
            $out = $cache->data;
          } 
          else {
            $data = _google_analytics_top_searches_get_feed($search_category_category);
            $out = _google_analytics_top_searchs_get_html_from_feed($data);
          }
        }
      }
    }
    else {
      $data = _google_analytics_top_searches_get_feed();
      $out = _google_analytics_top_searchs_get_html_from_feed($data);
    }
  }
  return $out;
}

/**
 * Interaction point to get results from Google Analytics API
 *
 * @param $category string
 *   Search category name.
 * @return object
 *   Feed object of query results
 */
function _google_analytics_top_searches_get_feed($category = NULL) {
  // Get max-results param value from user settings.
  if (!$number_of_results = variable_get('google_analytics_top_searches_number_searches')) {
    $number_of_results = 10;
  }

  //Get start date param value from user settings
  $start_date_timestamp = time() - 2592000; // Default to querying back 30 days.
  $start_date_choice = variable_get('google_analytics_top_searches_query_start_date');
 
  if (isset($start_date_choice)) {
    if ($start_date_choice === '0') {
      $start_date_timestamp = 1104559200; //January 1 2005, launch date of Google Analytics.
    } 
    else {
      $start_date_timestamp = time() - $start_date_choice;
    }
  }

  $params = array(
    'metrics' => 'ga:searchUniques',
    'dimensions' => 'ga:searchKeyword,ga:searchCategory',
    'sort_metric' => '-ga:searchUniques',
    'start_date' => $start_date_timestamp,
    'max_results' => $number_of_results
  );

  if ($category) {
    $params['filters'] = 'ga:searchCategory==' . $category;
  }

  $GAfeed = google_analytics_api_report_data($params);
  return $GAfeed;
}

/**
 * Get search categories from cache or google_analytics_top_searches_search_categories variable
 *
 * @return array
 *   array = (
 *    category_name => category_path,
 *    ...repeat as needed
 *   )
 */
function _google_analytics_top_searches_get_search_categories() {
  if ($cache = cache_get('google_analytics_top_searches_search_categories')) {
    $search_categories = $cache->data;
  }
  else {
    $search_categories = _google_analytics_get_search_categories_from_variable();
  }
  return $search_categories;
}

/**
 * Get search categories from google_analytics_top_searches_search_categories variable
 *
 * @return array
 *   array = (
 *    category_name => category_path,
 *    ...repeat as needed
 *   )
 */
function _google_analytics_get_search_categories_from_variable() {
  $paths = array();
  if($tracking_pages = variable_get('googleanalytics_category_tracking_pages')) {
    $paths_categories = preg_split ('/$\R?^/m', $tracking_pages);
    
    foreach ($paths_categories as $path_category_pair) {
      $pair_array = explode('|', $path_category_pair);
      $paths[trim($pair_array[1])] = trim($pair_array[0]);
    }
    //Set cache instead of variable so it can be cleared with cached content
    cache_set('google_analytics_top_searches_search_categories', $paths, 'cache', CACHE_TEMPORARY);
  }
  return $paths;
}

/**
 * Render HTML from query result feed object 
 *
 * @param object $feed
 *  result feed object
 * @return string 
 *  HTML unordered list of top search results
 */
function _google_analytics_top_searchs_get_html_from_feed($feed) {
  $categories = _google_analytics_top_searches_get_search_categories();
  $out = '<ul class="google-analytics-top-searches">';
  foreach ($feed->results as $result) {
    $out .= '<li>';
    $search_category = $result['searchCategory'];
    $link_path = '/' . str_replace('*', $result['searchKeyword'], $categories[$search_category]);
    $out .= '<a href="' . $link_path . '">' . $result['searchKeyword'] . '</a></li>';
  }
  $out .= '</ul>';
  return $out;
}